﻿var showLogs = true;
var keys;
var PlayerCharacter;


if (showLogs) {
    console.log("ScreenManager");
}

var gameWidth = 800;
var gameHeight = 450;
var backgroundColor = "#000000";
var game = new Phaser.Game(gameWidth, gameHeight, Phaser.Auto, 'KulturGameDev', { preload: preload, create: create, update: update });
var bFacingRight = true;

function preload() {
    if (showLogs) {
        console.log("ScreenManager - preload");
    }

    game.load.image('background', 'Pictures/bg.png');
    //game.load.image('mario', 'Pictures/mario.png'); animasyonsuz
    game.load.atlas('mario_walking', 'Sprites/MarioSprites.png', 'Sprites/MarioSprites.json', Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);
}

function create() {
    if (showLogs)
        console.log("ScreenManager - Create");
    var Background = game.add.sprite(game.world.centerX, game.world.centerY, 'background');
    Background.anchor.setTo(0.5, 0.5);

    //character = game.add.image(0, 0, 'mario'); eski

    keys = game.input.keyboard.createCursorKeys();

    PlayerCharacter = new GameObjects.Character();
    PlayerCharacter.init("Mario");

    //character = game.add.sprite(40, 345, 'mario_walking');
    //character.animations.add('walk');
    //character.animations.play('walk', 8, true);
    //character.anchor.setTo(0.5, 0.5);

    
}

function update() {
    /*
    if (keys.up.isDown) {
        character.y--;
        console.log("up");
    }
    else if (keys.down.isDown) {
        character.y++;
        console.log("down");
    }
    */
    if (keys.left.isDown) {
        PlayerCharacter.MoveLeft();

        //character.x--;
        //if (bFacingRight) {
        //    character.scale.x *= -1;
        //    bFacingRight = false;
        //}
        //console.log("left");
    }
    else if (keys.right.isDown) {
        PlayerCharacter.MoveRight();
        //character.x++;
        //if (!bFacingRight) {
        //    character.scale.x *= -1;
        //    //bFacingRight = true;
        //}
        //console.log("right");
    }

    else if (keys.up.isDown) {
        PlayerCharacter.Jump();
    }
    else {
        PlayerCharacter.Stand();
    }
}